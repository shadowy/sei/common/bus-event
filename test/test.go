package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/postgres/v2"
	"gitlab.com/shadowy/go/rabbitmq/v2"
	"gitlab.com/shadowy/sei/common/bus-event"
	"math/rand"
	"time"
)

const (
	sqlStart  = "select * from bus_event.view_events() as t"
	sqlDelete = "call bus_event.remove_event($1)"
)

type C struct {
}

func (c *C) GetDB() *postgres.Database {
	res := &postgres.Database{
		Host:     "shadowy.eu",
		Port:     5432,
		User:     "sei_email",
		Password: "sei_email",
		Db:       "sei_email",
		PoolSize: nil,
	}
	res.Init()
	return res
}

func (c *C) GetQueue() *rabbitmq.RabbitMq {
	return nil
}

func (c *C) GetEventBus() *busevent.EventBusSettings {
	return &busevent.EventBusSettings{
		DBChannel: "bus_event",
	}
}

func main() {
	cfg := &C{}
	listener, err := busevent.CreateListener(cfg)
	if err != nil {
		log.Logger.Error().Err(err).Stack().Msg("sss")
		return
	}
	err = listener.Start(sqlStart, sqlDelete)
	if err != nil {
		log.Logger.Error().Err(err).Stack().Msg("sss")
		return
	}

	for {
		var timeout = time.Duration(2000+rand.Intn(3000)) * time.Millisecond
		time.Sleep(timeout)
	}
}
