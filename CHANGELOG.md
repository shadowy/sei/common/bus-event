# CHANGELOG

<!--- next entry here -->

## 0.2.1
2021-06-11

### Fixes

- fix condition for sleep (aeb958ba04780ad7d7674891b111cc10f2f04f2f)

## 0.2.0
2021-06-11

### Features

- change flow for read events (a12d41e9715cda45286267f115baa5795612b8cb)

## 0.1.3
2021-06-11

### Fixes

- update library and fix listener (9031bc221cf1a74795969de9d6c3f794fe49009d)

## 0.1.2
2021-06-11

### Fixes

- fix db model (8f59a43df7d5394240c3d163b86b07212b456422)

## 0.1.1
2021-06-04

### Fixes

- define header for messages (0df4b54db19f11fac21e71e2e9198d95cd4217ba)

## 0.1.0
2021-06-03

### Features

- implement basic functionality (9dec779790ae14842578a482353646cfad630f9f)

