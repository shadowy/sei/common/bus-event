module gitlab.com/shadowy/sei/common/bus-event

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/rs/zerolog v1.22.0 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	gitlab.com/shadowy/go/postgres/v2 v2.0.2 // indirect
	gitlab.com/shadowy/go/rabbitmq/v2 v2.0.1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
