package busevent

import (
	"gitlab.com/shadowy/go/postgres/v2"
	"gitlab.com/shadowy/go/rabbitmq/v2"
)

type EventBusConfig interface {
	GetDB() *postgres.Database
	GetQueue() *rabbitmq.RabbitMq
}
