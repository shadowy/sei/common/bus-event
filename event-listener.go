package busevent

import (
	"database/sql"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"gitlab.com/shadowy/go/postgres/v2"
	"gitlab.com/shadowy/go/rabbitmq/v2"
	"gitlab.com/shadowy/sei/common/bus-event/model"
	"google.golang.org/protobuf/proto"
	"reflect"
	"time"
)

const (
	timeSleepOK    = 5
	timeSleepError = 10
)

type ExecutorForMessage func(source, documentType, action, data string) (proto.Message, error)

type Listener struct {
	log zerolog.Logger

	client    *postgres.Database
	sqlStart  string
	sqlDelete string
	output    *rabbitmq.Output

	executor map[string]ExecutorForMessage
}

func CreateListener(config EventBusConfig) (*Listener, error) {
	log.Logger.Debug().Msg("busevent.CreateListener")
	res := new(Listener)
	res.client = config.GetDB()
	res.executor = make(map[string]ExecutorForMessage)
	res.log = log.With().Logger()

	var err error
	res.output, err = config.GetQueue().GetOutput("")
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (listener *Listener) AddExecutor(documentType, action string, executor ExecutorForMessage) {
	listener.log.Debug().Str("documentType", documentType).Str("action", action).Msg("Listener.AddExecutor")
	listener.executor[documentType+":"+action] = executor
}

func (listener *Listener) Start(sqlStart, sqlDelete string) error {
	listener.log.Debug().Str("sqlStart", sqlStart).Str("sqlDelete", sqlDelete).Msg("Listener.AddExecutor")
	listener.sqlStart = sqlStart
	listener.sqlDelete = sqlDelete

	go listener.listen()

	return nil
}

func (listener *Listener) getEvents() ([]*model.Message, error) {
	listener.log.Debug().Msg("Listener.getEvents")
	list, err := listener.client.GetRowsModel(listener.sqlStart, reflect.TypeOf(model.MessageRow{}))
	if err != nil {
		return nil, err
	}
	var res []*model.Message
	for i := range list {
		item := list[i].(model.MessageRow)
		res = append(res, item.Event)
	}
	return res, nil
}

func (listener *Listener) send(event *model.Message) error {
	l := listener.log.With().
		Str("id", event.ID).
		Str("source", event.Source).
		Str("type", event.Type).
		Str("action", event.Action).
		Str("data", event.Data).
		Logger()
	l.Debug().Msg("Listener.send")
	executor := listener.getExecutor(event.Type, event.Action)
	if executor == nil {
		l.Warn().Msg("Listener.send executor not defined")
		return nil
	}
	msg, err := executor(event.Source, event.Type, event.Action, event.Data)
	if err != nil {
		l.Error().Err(err).Stack().Msg("Listener.send executor")
		return err
	}
	data, err := proto.Marshal(msg)
	if err != nil {
		l.Error().Err(err).Stack().Msg("Listener.send marshal")
		return err
	}

	header := amqp.Table{}
	header[HeaderDocumentType] = event.Type
	header[HeaderType] = "document"
	header[HeaderAction] = event.Action
	header[HeaderGID] = event.ID
	err = listener.output.SendWithHeader(data, header, 1)
	if err != nil {
		l.Error().Err(err).Stack().Msg("Listener.send send")
		return err
	}

	return nil
}

func (listener *Listener) getExecutor(documentType, action string) ExecutorForMessage {
	if cmd, ok := listener.executor[documentType+":"+action]; ok {
		return cmd
	}
	if cmd, ok := listener.executor[documentType+":*"]; ok {
		return cmd
	}
	if cmd, ok := listener.executor["*:*"]; ok {
		return cmd
	}
	return nil
}

func (listener *Listener) delete(eventID string) error {
	l := listener.log.With().Str("eventID", eventID).Logger()
	l.Debug().Msg("Listener.delete")
	return listener.client.UsingWithTransaction(func(tx *sql.Tx) error {
		errSQL := listener.client.ExecuteQuery(listener.sqlDelete, tx, eventID)
		if errSQL != nil {
			l.Error().Err(errSQL).Msg("Listener.delete")
		}
		return errSQL
	})
}

func (listener Listener) listen() {
	for {
		events, err := listener.getEvents()
		if err != nil {
			time.Sleep(time.Second * timeSleepError)
			continue
		}

		for i := range events {
			event := events[i]
			err = listener.send(event)
			if err != nil {
				continue
			}
			err = listener.delete(event.ID)
			if err != nil {
				continue
			}
		}
		if len(events) != 0 {
			continue
		}
		time.Sleep(time.Second * timeSleepOK)
	}
}
