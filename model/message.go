package model

type Message struct {
	ID     string `json:"id"`
	Source string `json:"source"`
	Type   string `json:"type"`
	Action string `json:"action"`
	Data   string `json:"data"`
}
